
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Sus datos y la seccion son los siguientes</h1>
        
        <%
            String nombre = (String) request.getAttribute("nombre");
            String seccion = (String) request.getAttribute("seccion");
        %>
        
        <p> Estimado <%=nombre%>, tu seccion es: <%=seccion%></p>
    </body>
</html>
